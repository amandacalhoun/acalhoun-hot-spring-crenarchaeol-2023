# ACalhoun Hot Spring Crenarchaeol 2023


## Name
ACalhoun Code for Crenarchaeol in Hot Springs Paper

## Description
The code and files included in this project are for open access data usage for samples included in the Calhoun yellowstone and compilation datasets associated with a published paper. 

## Questions?
Please contact acalhoun@g.harvard.edu with any questions or concerns!
